variable "name" {
  description = "The unique name to use."
  default     = "World"
  type        = string
}
