greeting = input('greeting', required: true)
filename = input('filename', required: true)

control 'greeting' do
  describe greeting do
    it { should eq "Hello, world!"}
  end

  describe file(filename) do
    it { should exist }
  end
end
