output "filename" {
  value = module.hello.filename
}

output "greeting" {
  value = module.hello.greeting
}