locals {
  name = "world"
}

module "hello" {
  source = "../../.."

  name = local.name
}