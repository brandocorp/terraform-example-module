locals {
  greeting = "Hello, ${var.name}!"
}

resource "local_file" "greeting" {
  content  = local.greeting
  filename = "${path.cwd}/greeting.txt"
}
