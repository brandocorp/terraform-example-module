output "filename" {
  description = "The greeting file."
  value       = local_file.greeting.filename
}

output "greeting" {
  description = "The completed greeting."
  value       = local.greeting
}